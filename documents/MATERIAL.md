# Materialliste:
| Name | Anzahl | Einzelpreis | Gesammtpreis | Status |
| ----- | ----- | ----- | ----- | ----- |
| Schrittmotoren (2x) | 1x | 6.99€ | 6.99€ | Vorhanden |
| Lochrasterplatten (10x) | 1x | 6.99€ | 6.99€ | Vorhanden |
| Arduino | 1x | 22.10€ | 22.10€ | Vorhanden |
| RGB-LED Strip WS2811 | 1x | 26.88€ | 26.88€ | Vorhanden |
| Zahnräder + Zahnriemen | 1x | 17.38€ | 17.38€ | Vorhanden |
| Raspberry Pi 3 Model B | 1x | 44.00€ | 44.00€ | Vorhanden |
| PVC-Platte (0,75m²) | 1x  | 21.98€ | 21.98€ | Vorhanden |
| Elektromagnet | 1x | 11.97€ | 11.97€ | Bestellt |
| 12V Netzteil | 1x | 7.99€ | 7.99€ | Vorhanden |
| Micro SD | 1x | 7.99€ | 7.99€ | Vorhanden |
| Jumper Kabel | 1x | 6.89€ | 6.89€ | Vorhanden |
| Breadboard | 1x | 9.99€ | 9.99€ | Vorhanden |
| Lötzinn 1mm | 1x | 6.14€ | 6.14€ | Vorhanden |
| Lötzinn 0.56mm | 1x | 7.27€ | 7.27€ | Vorhanden |
| Lötkolben | 1x | 14.99€ | 14.99€ | Vorhanden |
| Gabellichtschranke (10x) | 2x | 8.88€ | 17.76€ | Vorhanden |
| Widerstände | 1x | 6.99€ | 6.99€ | Vorhanden |
| Schachfiguren | 1x | 34.95€ | 34.95€ | Nicht Vorhanden |

Info: Die Schachfiguren werden erst nach erfolgreichen Tests gekauft