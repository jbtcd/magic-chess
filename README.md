# Zauberschach

## Projekt

Gewünscht ist die Möglichkeit eines automartisierten Schachfeldes, die Figuren sollen über ein Webinterface gesteuert werden und sich von alleine über das Spielfeld bewegen. Verschiedene Spiel-Modi sollen für mehr abwechslung sorgen (siehe unten). Das Spielfeld soll so leise und schnell wie nur Möglich Arbeiten.
Es ist drauf zu achten, das Spielfiguren vom Spielfeld geschoben werden, wenn sie vom Gegner geschlagen werden.
Auch wichtig ist, das wenn für einen Spielzug andere SPielfiguren, die im weg stehen umgangen oder verschoben werden um den Spielzug zu ermöglichen. Am Ende sollen die verschobenden Figuren wieder auf ihre Ausgangposition zurück.

## To do...
 - Wenn Spieler A Figur A2 auswählt und as Ziel ein Feld von Spieler A auswählt ändert das Ausgewähle Feld auf die neue Auswahl
 - Funktionen mit Kommentaren versehen
 - Code Objektorientiert schreiben
 - Text und Variablen Namen auf deutsch Übersetzen

 ### Must haves:
 - Schachfeld Gehäuse + Benötigte Schalter mit lichtfunktion
 - Figuren lassen sich vom Script bewegen
 - Sound-Einheit (Aux anschluss)
 - Berechnung der Möglichen Spielzüge
 - Webinterface zum Steuern der Spielzüge
 - Player vs. Player

### Nice to haves:
 - Sound-Einheit (BT)
 - Spracheingabe (z.B. B4 auf B6)
 - Player vs. Computer

## Spielmodi
| ID | Beschreibung |
| ------ | ------ |
| 1 | Normal: Spieler gegen Spieler, die Spielzüge werden sofort umgesetzt. |
| 2 | Hardcore: Spieler gegen Spieler, die Figuren Bewegen sich erst am Ende des Spieles. Ist ein Spielzug ungültig, wird der Spieler sofort aufgefordert Ihn zu wiederholen. Sobald ein Spieler Verliert oder X ungültige Spielzüge erreicht hat, wird das Spiel Virsualisiert. |
| 3 | Spieler gegen Computer |

**Copyrigth @ Jonah Böther**